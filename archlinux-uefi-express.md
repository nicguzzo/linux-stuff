
### LINUX-STUFF

>Desactivar Secure Boot desde el BIOS

Solo para red (wifi)
* ```root@archiso ~ # iwctl```
* ```[iwd]# device list```
* ```[iwd]# station <device> scan```
* ```[iwd]# station <device> get-networks```
* ```[iwd]# station <device> connect <SSID>```
* ```root@archiso ~ # ping google.com```
* ```cfdisk /dev/<device>``` 
* ```formatear mkfs.vfat -F32 /dev/<boot_device>```
* ```formatear mkfs.ext4 /dev/<root_device>```
* ```montar mount /dev/<device> /mnt```
* ```mkdir -p /mnt/boot```
* ```mount /dev/<boot_device> /mnt/boot```
* ```pacstrap /mnt base linux linux-firmware```
* ```genfstab -U /mnt >> /mnt/etc/fstab```
* ```arch-chroot /mnt```
* ```ln -sf /usr/share/zoneinfo/America/Argentina/San_Luis /etc/localtime```
* ```hwclock --systohc``` o ```hwclock --hctosys```
* ```pacman -S nanogrub os-prober efibootmgr ntfs-3g dmidecode```
 ```
echo "[Match]\
Name=<device name>\
[Network]\
DHCP=yes" >/etc/systemd/network/20-wired.network
```
* ```systemd enable systemd-networkd```
* ```systemd enable systemd-resolved```
* ```nano /etc/locale.gen```
* ```locale-gen```
* ```nano /etc/hostname```
* ```passwd```
* ```pacman -S intel-ucode``` o ```pacman -S amd-ucode```
* ```grub-install --target=x86_64-efi --efi-directory=/efi --bootloader-id=GRUB```
* ```root@archiso ~ # grub-mkconfig -o /boot/grub/grub.cfg```
* ```umount /mnt/efi```
* ```umount /mnt```
* ```reboot```
