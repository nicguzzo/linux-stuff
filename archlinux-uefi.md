### LINUX-STUFF
Desactivar Secure Boot desde el BIOS

Bajar iso de ArchLinux 
>https://www.archlinux.org/download/

Bootear el iso de archlinux

```root@archiso ~ # localectl set-x11-keymap <latam / es / en>``` 

Probar conexion
```root@archiso ~ # ping google.com```

Solo para red (wifi)
```root@archiso ~ # ls /sys/class/net```

>Si no existe ningún fichero que inicie con la letra w ,entonces no podrá realizar la instalación vía wifi, si no solo vía cable ethernet. Si por el contrario el fichero existe entonces será posible conectarse a Internet vía wifi, para eso usaremos la herramienta iwctl.

```root@archiso ~ # iwctl```

>Listamos los dispositivos:
>```[iwd]# device list```
>Luego, para buscar redes:
>```[iwd]# station <device> scan```
>Para ver las redes encontradas:
>```[iwd]# station <device> get-networks```
>Ahora nos conectamos a la red:
>```[iwd]# station <device> connect <SSID>```

Esto nos tendria que dar ip, sino editar /etc/iwd/main.conf

```
[General]
EnableNetworkConfiguration=true
```

Probar conexion
```root@archiso ~ # ping google.com```

**Verificamos el modo de arranque**

>Verificamos si la placa base es compatible con UEFI, consultando si existe el directorio especificado y mostrando resultado con archivos existentes, caso contrario de no mostrar información ni archivos el arranque solo es compatible con BIOS/Legacy.

```root@archiso ~ # ls /sys/firmware/efi/efivars```

Particionar el disco

Veamos los discos y las particiones
```root@archiso ~ # fdisk -l```

**Example layouts**

BIOS with MBR

| Mount point | Partition | Partition type | Suggested size |
| --- | --- | --- | --- |
| [SWAP] | /dev/swap_partition | Linux swap | More than 512 MiB |
| /mnt | /dev/root_partition | Linux	| Remainder of the device |

UEFI with GPT

| Mount point | Partition | Partition type | Suggested size |
| ------ | ------ | ------ | ------ |
| /mnt/boot or /mnt/efi | /dev/efi_system_partition | EFI system partition |	At least 260 MiB |
| [SWAP] | /dev/swap_partition | Linux swap | More than 512 MiB |
| /mnt	| /dev/root_partition | Linux x86-64 root (/) |	Remainder of the device |


Si necesitamos borrar o crear alguna
```root@archiso ~ # cfdisk /dev/<device>``` 
>***Usar particion gpt disco completo, o 2 particiones raiz y swap***

Formatear
```root@archiso ~ # formatear mkfs.ext4 /dev/<device>```

Montar
```root@archiso ~ # montar mount /dev/<device> /mnt```

Activar SWAP (Solo si creaste una particion swap)
```root@archiso ~ # swapon /dev/swap_partition```

**Instalar paquetes esenciales**
```root@archiso ~ # pacstrap /mnt base linux linux-firmware```

Generate an fstab file (use -U or -L to define by UUID or labels, respectively):

>En de tener Windows instalado, podemos usar la misma partion EFI
```root@archiso ~ # mkdir -p /mnt/efi```
```root@archiso ~ # mount /dev/<device-efi> /mnt/efi```

Generar el archivo fstab (use -U o -L para definir por UUID o etiquetas, respectivamente)

```root@archiso ~ # genfstab -U /mnt >> /mnt/etc/fstab```

**Chroot**
Change root into the new system:

```root@archiso ~ # arch-chroot /mnt```

Establecer la zona horaria:
```root@archiso ~ # ln -sf /usr/share/zoneinfo/Region/City /etc/localtime```

>Ejemplo:
>```root@archiso ~ # ln -sf /usr/share/zoneinfo/America/Argentina/San_Luis /etc/localtime```

Sincronizar la hora
```root@archiso ~ # hwclock --systohc``` o ```root@archiso ~ # hwclock --hctosys```

Crear y editar nano /etc/systemd/network/20-wired.network

Agregue en el archivo /etc/hostname:
```
[Match]
Name=<device name>
[Network]
DHCP=yes
```

Habilitar servicios de red
```
systemd enable systemd-networkd
systemd enable systemd-resolved
```


Necesitamos editar archivos, por tal motivo vamos a instalar nano
```root@archiso ~ # pacman -S nano```

Edite /etc/locale.gen y descomente en_US.UTF-8 UTF-8 y otras configuraciones regionales necesarias. Genere las configuraciones regionales ejecutando:

```root@archiso ~ # nano /etc/locale.gen```
```root@archiso ~ # locale-gen```

Creamos y editamos el archivo hostname:
```root@archiso ~ # nano /etc/hostname```


Agregue en el archivo /etc/hostname:
```myhostname```

>myhostname, reemplazar por el nombre del host

Editamos el archivo hosts:
```root@archiso ~ # nano /etc/hosts```

Agregar las siguientes lineas al archivo /etc/hosts:

```
127.0.0.1	localhost
::1		localhost
127.0.1.1	myhostname.localdomain	myhostname
```

Crear password para el usuario root
```root@archiso ~ # passwd```

>Dependiendo si su procesador en AMD o INTEL
```root@archiso ~ # pacman -S intel-ucode``` o ```root@archiso ~ # pacman -S amd-ucode```

A continuacion, instalamos
```root@archiso ~ # pacman -S grub os-prober efibootmgr ntfs-3g dmidecode```

Instalar gestor de arranque

>La forma mas clasica, ```root@archiso ~ # grub-install /dev/sda```

```root@archiso ~ # grub-install --target=x86_64-efi --efi-directory=/efi --bootloader-id=GRUB```

```root@archiso ~ # grub-mkconfig -o /boot/grub/grub.cfg```

Para finalizar, desmontamos y reiniciamos
```
root@archiso ~ # umount /mnt/efi
root@archiso ~ # umount /mnt
root@archiso ~ # reboot
```
